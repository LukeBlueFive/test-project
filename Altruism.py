#test comment
def score(dice):
    points = 0
    oneCount = 0
    twoCount = 0
    threeCount = 0
    fourCount = 0
    fiveCount = 0
    sixCount = 0

    for roll in dice:
        if (roll == 1):
            oneCount = oneCount + 1
        elif (roll == 2):
            twoCount = twoCount + 1
        elif (roll == 3):
            threeCount = threeCount + 1
        elif (roll == 4):
            fourCount = fourCount + 1
        elif (roll == 5):
            fiveCount = fiveCount + 1
        elif (roll == 6):
            sixCount = sixCount + 1

    if (oneCount >= 3):
        points = points + 1000
        oneCount = oneCount - 3
    elif (sixCount >= 3):
        points = points + 600
    elif (fiveCount >= 3):
        points = points + 500
        fiveCount = fiveCount - 3
    elif (fourCount >= 3):
        points = points + 400
    elif (threeCount >= 3):
        points = points + 300
    elif (twoCount >= 3):
        points = points + 200
    
    if (oneCount >= 1):
        points = points + (oneCount * 100)
    if (fiveCount >= 1):
        points = points + (fiveCount * 50)

    return points

print(score( [2, 3, 4, 6, 2] ))
print(score( [4, 4, 4, 3, 3] ))
print(score( [2, 4, 4, 5, 4] ))
